<?php
/**
 * Random PHP stretch questions
 * ============================
 *
 * A web service to give you a random question to ask in your 1-on-1s
 */

DEFINE('BASE_PATH', dirname(__FILE__));
$_GET = [];

$queries = explode('&', $_SERVER['QUERY_STRING'] ?? '');

if ( $queries ) {
	foreach( $queries as $query ) {
		list($k, $v) = explode('=', $query);
		$_GET[$k] = $v;
	}
}

$question_json = json_decode(file_get_contents(CONSTANT('BASE_PATH') . '/questions.json'), true);
$questions = $question_json['questions'] ?? [];
$protocol = 'http';

if ( $_SERVER['HTTPS'] ?? '' ) {
	$protocol = 'https';
}

$baseURL = "${protocol}://" . $_SERVER['HTTP_HOST'];

$questionCount = count( $questions );
if ( isset( $_GET['index'] ) && is_numeric( $_GET['index'] ) ) {
	$index = (int) $_GET['index'];
} else {
	$index = mt_rand( 0,  $questionCount );
	header( "Location: ${baseURL}?index=${index}" );
}

if ( isset( $index ) && is_int($index) && $index >=0 && $index < $questionCount ) {
	$question = $questions[ $index ];
} else {
	header( "Location: ${baseURL}" );
}
?>
<!doctype html>
<head>
	<meta charset=utf8 />
	<meta http-eqiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>🎱 Stretch Questions</title>
	<style>
		body {
			font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif;
			line-height: 3em;
			font-size: 27px;
			color: #333;
			max-width: 85%;
			margin: 3em auto;
			text-align: center;
		}
		p {
			width: 40rem;
			margin: 2em auto;
			line-height: 1.5em
		}
		header {
			margin: 0;
			overflow: hidden;
		}
		header .logo {
			padding: 0.25em 1em;
			float: left;
			font-size: 1em;
			font-weight: 300;
		}
		header nav {
			float: right;
		}
		header:after {
			content: '';
			height: 3px;
			display: block;
			background-image: linear-gradient(to right, #990000 15%, #006699 15%, #006699 85%, #339966 85%);
			clear: both;
		}
		input[type="submit"] {
			font-size: 1.1rem;
			padding: 1.5em 2em;
		}
		footer {
			margin: 5em;
			border-top: 1px solid #ccc;
			padding: 2em;
			font-size: small;
		}
	</style>
</head>
<body>
	<header>
		<h1 class="logo">🎱 Your special random 1-on-1 question is...</h1>
	</header>
	<div class="main">
		<p><?= htmlentities( $question ); ?></p>

		<form action="<?= htmlentities( $baseUrl ); ?>">
			<input type="submit" value="Gimme another" />
		</form>
	</div>

<footer>
	<div id="powered-by">
	<a href="https://admin.toolforge.org/"><img src="https://tools-static.wmflabs.org/toolforge/banners/Powered-by-Toolforge.png" alt="Powered by Wikimedia Toolforge"></a>
	</div>
		<a id="source" href="https://gitlab.wikimedia.org/thcipriani/random-stretch-question">view source</a>
	</footer>
</body>
